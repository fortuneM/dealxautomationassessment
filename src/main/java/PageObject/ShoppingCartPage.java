package PageObject;

import Common.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class ShoppingCartPage extends BasePage {

    @FindBy(xpath = "//*[@id=\"cart_quantity_up_1_1_0_0\"]/span")
    private WebElement btnIncreaseQuantity;

    @FindBy(id = "total_product_price_1_1_0")
    private WebElement lblTotalProductsPrice;

    public ShoppingCartPage(WebDriver wDriver) {
        super();
        PageFactory.initElements(wDriver, this);
    }

    /**
     * Description: This method clicks on Increase QTY button
     */
    public boolean clickIncreaseQtyButton() {
        wDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        if (btnIncreaseQuantity.isDisplayed()) {
            btnIncreaseQuantity.click();
            return true;
        }
        return false;
    }

    /**
     * Description: This method verifies that the displayed total matches calculated total.
     */
    public boolean verifyDisplayedPrice(int qty) throws InterruptedException {
        wDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        Thread.sleep(2000);
        double calculatedPrice = CalculatePrice(productPrice, qty);
        double displayedPrice = Double.parseDouble(lblTotalProductsPrice.getText().replace("$", ""));
        if (Double.compare(calculatedPrice, displayedPrice) == 0) {
            return true;
        }
        return false;
    }
}

package PageObject;

import Common.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class SignInPage extends BasePage {

    @FindBy(id = "email")
    private WebElement inputEmail;

    @FindBy(id = "passwd")
    private WebElement inputPassword;

    @FindBy(id = "SubmitLogin")
    private WebElement btnSubmitLogin;

    public SignInPage(WebDriver wDriver) {
        super();
        PageFactory.initElements(wDriver, this);
    }

    /**
     * Description: This method captures login email
     */
    public boolean captureUsername(String usrname) {
        wDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        if (inputEmail.isDisplayed()) {
            inputEmail.sendKeys(usrname);
            return true;
        }
        return false;
    }

    /**
     * Description: This method captures login password
     */
    public boolean capturePassword(String password) {
        wDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        if (inputPassword.isDisplayed()) {
            inputPassword.sendKeys(password);
            return true;
        }
        return false;
    }

    /**
     * Description: This method click login button
     */
    public boolean clickLoginButton() {
        wDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        if (btnSubmitLogin.isDisplayed()) {
            btnSubmitLogin.click();
            return true;
        }
        return false;
    }


}

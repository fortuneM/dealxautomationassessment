package PageObject;

import Common.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class SearchPage extends BasePage {

    @FindBy(xpath = "//*[@id=\"center_column\"]/ul/li[1]/div/div[2]/h5/a")
    private WebElement lblSearchResult;

    public SearchPage(WebDriver wDriver) {
        super();
        PageFactory.initElements(wDriver,this);
    }

    /**
     * Description: This method verifies the searched product
     */
    public boolean verifySearchedProduct(String product) throws InterruptedException {
        wDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        Thread.sleep(3000);
        if(lblSearchResult.getText().contains(product)){
            return true;
        }
        return false;
    }

}

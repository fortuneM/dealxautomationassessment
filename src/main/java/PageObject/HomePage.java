package PageObject;

import Common.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class HomePage extends BasePage {

    @FindBy(id = "search_query_top")
    private WebElement inputSearchBox;

    @FindBy(name = "submit_search")
    private WebElement btnSubmitSearch;

    @FindBy(className = "login")
    private WebElement btnLogin;

    @FindBy(xpath = "//*[@id=\"homefeatured\"]/li[1]/div/div[2]/div[2]/a[1]/span")
    private WebElement btnAddToCart;

    @FindBy(xpath = "//*[@id=\"layer_cart\"]/div[1]/div[2]/div[1]/span")
    private WebElement lblPricePerProduct;

    @FindBy(xpath = "//*[@id=\"homefeatured\"]/li[1]")
    private WebElement hoverOverProduct;

    @FindBy(xpath = "//*[@id=\"block_top_menu\"]/ul/li[1]/a")
    private WebElement hoverOverWomenCategory;

    @FindBy(xpath = "//*[@id=\"block_top_menu\"]/ul/li[1]/ul/li[1]/ul/li[1]/a")
    private WebElement lnkBtnTshirtSubCategory;

    @FindBy(className = "category-name")
    private WebElement lblCategoryPage;

    @FindBy(xpath = "//*[@id=\"layer_cart\"]/div[1]/div[2]/div[4]/a")
    private WebElement btnProceedToCheckout;

    @FindBy(xpath = "//*[@id=\"header_logo\"]/a/img")
    private WebElement lblLogo;

    public HomePage(WebDriver wDriver) {
        super();
        PageFactory.initElements(wDriver, this);
    }

    /**
     * Description: This method captures the product to search
     */
    public boolean captureProductToSearch(String productName) {
        wDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        if (inputSearchBox.isDisplayed()) {
            inputSearchBox.sendKeys(productName);
            return true;
        }
        return false;
    }

    /**
     * Description: This method clicks search submit button
     */
    public boolean clickSearchSubmitButton() {
        wDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        if (btnSubmitSearch.isDisplayed()) {
            btnSubmitSearch.click();
            return true;
        }
        return false;
    }

    /**
     * Description: This method verifies landing page
     */
    public boolean verifyLandingPage() {
        wDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        if (lblLogo.isDisplayed()) {
            return true;
        }
        return false;
    }

    /**
     * Description: This method clicks on Logo button
     */
    public boolean clickLogoButton() {
        wDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        if (lblLogo.isDisplayed()) {
            lblLogo.click();
            return true;
        }
        return false;
    }

    /**
     * Description: This method get the product price and Clicks Add to cart button
     */
    public boolean clickAddToCartButton() {
        wDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        Actions action = new Actions(wDriver);
        action.moveToElement(hoverOverProduct).perform();
        /*try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
        if (btnAddToCart.isDisplayed()) {
            btnAddToCart.click();
            return true;
        }
        return false;
    }

    /**
     * Description: This method clicks on Proceed to checkout button
     */
    public boolean clickProceedToCheckout() throws InterruptedException {
        wDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        Thread.sleep(2000);
        if (btnProceedToCheckout.isDisplayed()) {
            Thread.sleep(1000);
            productPrice = Double.parseDouble(lblPricePerProduct.getText().replace("$",""));
            Thread.sleep(1000);
            btnProceedToCheckout.click();
            return true;
        }
        return false;
    }

    /**
     * Description: This method clicks on sing in button
     */
    public boolean clickSignInButton() {
        wDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        if (btnLogin.isDisplayed()) {
            btnLogin.click();
            return true;
        }
        return false;
    }

    /**
     * Description: This method hovering over Women Category clicks on sub category
     */
    public boolean hoverOverClickCategory() {
        wDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        if (hoverOverWomenCategory.isDisplayed()) {
            Actions action = new Actions(wDriver);
            action.moveToElement(hoverOverWomenCategory).perform();
            lnkBtnTshirtSubCategory.click();
            return true;
        }
        return false;
    }

    /**
     * Description: This method verifies that Category page is loaded
     */
    public boolean verifyCategoryPage() {
        wDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        if (lblCategoryPage.isDisplayed()) {
            return true;
        }
        return false;
    }

}

package Common;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class BasePage {

    public static WebDriver wDriver;
    public  static  ArrayList dataList = new ArrayList();
    public static  final String USERNAME = "fortuemoney@gmail.com";
    public static  final String PASSWORD = "Testing123";
    public static double productPrice = 0.00;


    public void openBrowser() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/webdrivers/chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("useAutomationExtension", false);
        wDriver = new ChromeDriver();
        wDriver.manage().window().maximize();
        wDriver.get("http://automationpractice.com/");
    }

    public void readTestData(){

        try {
            File myObj = new File("src/main/resources/testdata/products.txt");
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                dataList.add(data);
                System.out.println(data);
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    public double CalculatePrice(double unitPrice, int quantity){
        return unitPrice*quantity;
    }

}

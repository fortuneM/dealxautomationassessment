package stepdefs;

import Common.BasePage;
import PageObject.HomePage;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import org.junit.After;
import org.junit.Assert;

public class CommonStepdefs extends BasePage {
    HomePage homePage = new HomePage(wDriver);

    @Given("As a user i launch web application$")
    public void asAUserILaunchWebApplication() {
        System.out.println("Opening Browser and launching application...");
        openBrowser();
    }

    @And("^I land on automation practice Home page$")
    public void iLandOnAutomationPracticeLoginPage() {
        homePage = new HomePage(wDriver);
        Assert.assertTrue( "Unable to verify landing page",homePage.verifyLandingPage());
    }

}

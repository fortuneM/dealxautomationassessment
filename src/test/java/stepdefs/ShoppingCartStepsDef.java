package stepdefs;

import Common.BasePage;
import PageObject.ShoppingCartPage;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import org.junit.Assert;

public class ShoppingCartStepsDef extends BasePage {
    ShoppingCartPage shoppingCartPage = new ShoppingCartPage(wDriver);

    @And("^I increase quantity to a desired amount \"([^\"]*)\"$")
    public void iIncreaseQuantityToADesiredAmount(String qty) throws Throwable {
        int quantity = Integer.parseInt(qty);
        for (int x = 1; x < quantity; x++) {
            Assert.assertTrue("Unable to click login button", shoppingCartPage.clickIncreaseQtyButton());
        }
    }

    @Then("^I Verify displayed total matches calculated total \"([^\"]*)\"$")
    public void iVerifyDisplayedTotalMatchesCalculatedTotal(String qty) throws Throwable {
        Assert.assertTrue("Unable to verify displayed total with calculated total.", shoppingCartPage.verifyDisplayedPrice(Integer.parseInt(qty)) );
    }
}

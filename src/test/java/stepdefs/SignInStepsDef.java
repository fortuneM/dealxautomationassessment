package stepdefs;

import Common.BasePage;
import PageObject.SignInPage;
import org.junit.Assert;
import cucumber.api.java.en.And;

public class SignInStepsDef extends BasePage {

    SignInPage signInPage = new SignInPage(wDriver);

    @And("^I enter username \"([^\"]*)\" and password \"([^\"]*)\"$")
    public void iEnterUsernameAndPassword(String arg0, String arg1) throws Throwable {
        Assert.assertTrue("Unable to capture username", signInPage.captureUsername(BasePage.USERNAME));
        Assert.assertTrue("Unable to capture password", signInPage.capturePassword(BasePage.PASSWORD));
    }

    @And("I click login button$")
    public void iClickLoginButton() {
        Assert.assertTrue("Unable to click login button", signInPage.clickLoginButton());
    }

}

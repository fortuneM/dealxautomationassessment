package stepdefs;

import Common.BasePage;
import PageObject.HomePage;
import PageObject.SearchPage;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

import java.util.ArrayList;

public class HomePageStepsDef extends BasePage {
    HomePage homePage = new HomePage(wDriver);
    SearchPage searchPage = new SearchPage(wDriver);

    @When("^I capture product to search \"([^\"]*)\"$")
    public void iCaptureProductToSearch(String productName) throws Throwable {
        Assert.assertTrue("Unable to capture product to search", homePage.captureProductToSearch(productName));
    }

    @And("^I click submit button$")
    public void iClickSubmitButton() {
        Assert.assertTrue("Unable to click search submit button", homePage.clickSearchSubmitButton());
    }


    @Then("^I search and validate from one variable separated by commas\"([^\"]*)\"$")
    public void iSearchAndValidateFromOneVariableSeparatedByCommas(String productString) throws Throwable {
        String[] token = productString.split(",");
        for (int x = 0; x < token.length; x++) {
            Assert.assertTrue("Unable to capture product to search - product: " + token[x], homePage.captureProductToSearch(token[x]));
            Assert.assertTrue("Unable to click search submit button - product: " + token[x], homePage.clickSearchSubmitButton());
            Assert.assertTrue("Unable to verify Search Results - product: " + token[x], searchPage.verifySearchedProduct(token[x]));
            Assert.assertTrue("Unable to click logo button - product: " + token[x], homePage.clickLogoButton());

        }
    }

    @Then("^I search and validate from external datafile$")
    public void iSearchAndValidateFromExternalDatafile() throws InterruptedException {
        readTestData();
        ArrayList<String> productsList = dataList;
        for (int x = 0; x < productsList.size(); x++) {
            Assert.assertTrue("Unable to capture product to search - product: " + productsList.get(x), homePage.captureProductToSearch(productsList.get(x)));
            Assert.assertTrue("Unable to click search submit button - product: " + productsList.get(x), homePage.clickSearchSubmitButton());
            Assert.assertTrue("Unable to verify Search Results - product: " + productsList.get(x), searchPage.verifySearchedProduct(productsList.get(x)));
            Assert.assertTrue("Unable to click logo button - product: " + productsList.get(x), homePage.clickLogoButton());

        }
    }

    @When("^I click on Sign In link button$")
    public void iClickOnSignInLinkButton() {
        Assert.assertTrue("Unable to click sign in button", homePage.clickSignInButton());
    }

    @When("^I click add to cart$")
    public void iClickAddToCart() {
        Assert.assertTrue("Unable to click add to cart", homePage.clickAddToCartButton());
    }

    @And("^I click proceed to checkout$")
    public void iClickProceedToCheckout() throws InterruptedException {
        Assert.assertTrue("Unable to click proceed to checkout", homePage.clickProceedToCheckout());
    }

    @When("^I hover over main category and click the sub category$")
    public void iHoverOverMainCategoryAndClickTheSubCategory() {
        Assert.assertTrue("Unable to hover and click sub category", homePage.hoverOverClickCategory());
    }

    @Then("^I verify that the page has loaded$")
    public void iVerifyThatThePageHasLoaded() {
        Assert.assertTrue("Unable to verify category page", homePage.verifyCategoryPage());
    }
}

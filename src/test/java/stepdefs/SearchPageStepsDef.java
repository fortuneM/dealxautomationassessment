package stepdefs;

import Common.BasePage;
import PageObject.SearchPage;
import cucumber.api.PendingException;
import cucumber.api.java.en.Then;
import org.junit.Assert;

public class SearchPageStepsDef extends BasePage {
    SearchPage searchPage = new SearchPage(wDriver);

    @Then("^I verify that the first result matches search criteria \"([^\"]*)\"$")
    public void iVerifyThatTheFirstResultMatchesSearchCriteria(String productName) throws Throwable {
        Assert.assertTrue("Unable to verify Search Results", searchPage.verifySearchedProduct(productName) );
    }
}

@runAll
@Navigate
Feature: Navigation Menu

  @NavigationMenu
  Scenario Outline: Search and Validate a product
    Given As a user i launch web application
    And I land on automation practice Home page
    When I hover over main category and click the sub category
    Then I verify that the page has loaded

    Examples:
      | productName |  |  |
      | zzzzz       |  |  |

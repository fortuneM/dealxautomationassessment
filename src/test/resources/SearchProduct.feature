@runAll
@Search
Feature: Search for a product and validate

  @SearchProduct
  Scenario Outline: Search and Validate a product
    Given As a user i launch web application
    And I land on automation practice Home page
    When I capture product to search "<productName>"
    And I click submit button
    Then I verify that the first result matches search criteria "<productName>"

    Examples:
      | productName |  |  |
      | Dress       |  |  |

  @SearchCommaProduct
  Scenario Outline: Store 3 search criteria in one variable separated by commas
    Given As a user i launch web application
    When I land on automation practice Home page
    Then I search and validate from one variable separated by commas"<productName>"

    Examples:
      | productName         |  |  |
      | Dress,shirts,Blouse |  |  |

  @SearchProductFromExternalDatafile
  Scenario Outline: search and validate products from external datafile
    Given As a user i launch web application
    When I land on automation practice Home page
    Then I search and validate from external datafile

    Examples:
      | productName |  |  |
      |             |  |  |

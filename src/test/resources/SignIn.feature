@runAll
@SignIn
Feature: Sign into the website using a username and password

  @SignInUsingGlobalVariable
  Scenario Outline: Sign into the website using a username and password stored as a global variable
    Given As a user i launch web application
    And I land on automation practice Home page
    When I click on Sign In link button
    And I enter username "<username>" and password "<password>"
    And I click login button

    Examples:
      | username | password |  |
      | usrname  | Testing123  |  |
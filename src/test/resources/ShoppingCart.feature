@runAll
@ShoppingCart
Feature: Add an item to shopping your cart

  @addToCart
  Scenario Outline: Add an item to shopping your cart
    Given As a user i launch web application
    And I land on automation practice Home page
    When I click add to cart
    And I click proceed to checkout
    And I increase quantity to a desired amount "<quantity>"
    Then I Verify displayed total matches calculated total "<quantity>"

    Examples:
      | quantity |  |  |
      | 3        |  |  |
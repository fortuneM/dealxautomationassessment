$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("NavigationMenu.feature");
formatter.feature({
  "line": 3,
  "name": "Navigation Menu",
  "description": "",
  "id": "navigation-menu",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@runAll"
    },
    {
      "line": 2,
      "name": "@Navigate"
    }
  ]
});
formatter.scenarioOutline({
  "line": 6,
  "name": "Search and Validate a product",
  "description": "",
  "id": "navigation-menu;search-and-validate-a-product",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 5,
      "name": "@NavigationMenu"
    }
  ]
});
formatter.step({
  "line": 7,
  "name": "As a user i launch web application",
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "I land on automation practice Home page",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "I hover over main category and click the sub category",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "I verify that the page has loaded",
  "keyword": "Then "
});
formatter.examples({
  "line": 12,
  "name": "",
  "description": "",
  "id": "navigation-menu;search-and-validate-a-product;",
  "rows": [
    {
      "cells": [
        "productName",
        "",
        ""
      ],
      "line": 13,
      "id": "navigation-menu;search-and-validate-a-product;;1"
    },
    {
      "cells": [
        "zzzzz",
        "",
        ""
      ],
      "line": 14,
      "id": "navigation-menu;search-and-validate-a-product;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 14,
  "name": "Search and Validate a product",
  "description": "",
  "id": "navigation-menu;search-and-validate-a-product;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 5,
      "name": "@NavigationMenu"
    },
    {
      "line": 2,
      "name": "@Navigate"
    },
    {
      "line": 1,
      "name": "@runAll"
    }
  ]
});
formatter.step({
  "line": 7,
  "name": "As a user i launch web application",
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "I land on automation practice Home page",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "I hover over main category and click the sub category",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "I verify that the page has loaded",
  "keyword": "Then "
});
formatter.match({
  "location": "CommonStepdefs.asAUserILaunchWebApplication()"
});
formatter.result({
  "duration": 14294335200,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepdefs.iLandOnAutomationPracticeLoginPage()"
});
formatter.result({
  "duration": 131409400,
  "status": "passed"
});
formatter.match({
  "location": "HomePageStepsDef.iHoverOverMainCategoryAndClickTheSubCategory()"
});
formatter.result({
  "duration": 3706158000,
  "status": "passed"
});
formatter.match({
  "location": "HomePageStepsDef.iVerifyThatThePageHasLoaded()"
});
formatter.result({
  "duration": 127057700,
  "status": "passed"
});
});